import { Post } from './../../models/post/post';
import { PostComponent } from './../post/post.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackBarService } from '../../services/snack-bar.service';
import { GyazoService } from 'src/app/services/gyazo.service';

@Component({
  selector: 'app-post-editor-dialog',
  templateUrl: './post-editor-dialog.component.html',
  styleUrls: ['./post-editor-dialog.component.sass']
})
export class PostEditorDialogComponent implements OnInit {
  public post: Post;
  public body: string;
  public img: string;
  public imageUrl: string;
  public imageFile: File;

  constructor(
    public dialogRef: MatDialogRef<PostComponent>,
    private snackBarService: SnackBarService,
    private gyazoService: GyazoService,
    @Inject(MAT_DIALOG_DATA) public data: Post) { }

  ngOnInit(): void {
    this.body = this.data.body;
    this.img = this.data.previewImage;
  }

  public ngOnDestroy() {
    this.body;
    this.img;
  }

  onNoClick(): void {
    this.dialogRef.close(this.data);
  }

  public loadImage(target: any) {
    this.imageFile = target.files[0];

    if (!this.imageFile) {
      target.value = '';
      return;
    }

    if (this.imageFile.size / 1000000 > 5) {
      target.value = '';
      this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
      return;
    }

    const reader = new FileReader();
    reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
    reader.readAsDataURL(this.imageFile);

    this.gyazoService.uploadImage(this.imageFile)
      .subscribe((imageData) => {
        this.img = imageData.url;
      });
  }

  public removeImage() {
    this.img = undefined;
    this.imageUrl = undefined;
    this.imageFile = undefined;
  }

  public SavePost() {
    this.post = {
      id: this.data.id,
      createdAt: this.data.createdAt,
      author: this.data.author,
      previewImage: this.img,
      body: this.body,
      comments: this.data.comments,
      reactions: this.data.reactions
    }
    this.data = this.post;
    this.onNoClick();
  }
}
