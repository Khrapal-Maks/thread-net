import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { SnackBarService } from '../../services/snack-bar.service';
import { environment } from 'src/environments/environment';
import { GyazoService } from 'src/app/services/gyazo.service';
import { MatDialog } from '@angular/material/dialog';
import { PostEditorDialogComponent } from '../post-editor-dialog/post-editor-dialog.component';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;

    public currentUser: User;
    public currentPost: Post;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        public dialog: MatDialog
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.authService.userOff.pipe(takeUntil(this.unsubscribe$)).subscribe(() => this.refreshPosts(this.isOnlyMine = false))

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public refreshPosts(isOnly: boolean) {
        if (isOnly == true) {
            this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
        }
        else {
            this.posts = this.cachedPosts;
        }
    }

    public openEditDialog(post: Post): void {
        const oldPost = post;
        const dialogRef = this.dialog.open(PostEditorDialogComponent, {
            width: '450px',
            data: post
        });

        dialogRef.afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(result => {
                if (oldPost != result) {
                    this.updatePost(result);
                }
            });
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public updatePost(post: Post) {
        this.postService.updatePost(post.id, post).subscribe(
            (respPost) => {
                this.updatedPost(respPost.body);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public deletePost(post: Post) {
        this.postService.deletePost(post.id).subscribe((data) => {
            if (data.status == 200) {
                this.deletedPost(post);
                this.refreshPosts(this.isOnlyMine);
            }
            else {
                () => this.snackBarService.showErrorMessage(data.status)
            }
        });
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public sliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.refreshPosts(this.isOnlyMine = true);
        } else {
            this.refreshPosts(this.isOnlyMine = false);
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/post`).build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.postHub.on('DeletedPost', (post: Post) => {
            if (post) {
                this.deletedPost(post);
            }
        });

        this.postHub.on('UpdatedPost', (post: Post) => {
            if (post) {
                this.updatedPost(post);
            }
        });
    }

    public updatedPost(post: Post) {
        if (this.cachedPosts.some((x) => x.id == post.id)) {
            const indexUpdate = this.cachedPosts.indexOf(this.cachedPosts.find((x) => x.id == post.id));
            this.cachedPosts[indexUpdate].body = post.body;
            this.cachedPosts[indexUpdate].previewImage = post.previewImage;
            this.sortPostArray(this.posts = this.cachedPosts);
        }
    }

    public deletedPost(post: Post) {
        if (this.cachedPosts.some((x) => x.id == post.id)) {
            const indexDelete = this.cachedPosts.indexOf(post);
            this.cachedPosts.splice(indexDelete, 1);
            this.sortPostArray(this.posts = this.cachedPosts);
        }
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        const result = array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
        this.refreshPosts(this.isOnlyMine);
        return result;
    }
}
