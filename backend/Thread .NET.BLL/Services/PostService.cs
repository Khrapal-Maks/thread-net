﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private Image _image;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetPost(int id)
        {
            var post = await _context.Posts
               .FirstAsync(post => post.Id == id);

            var postDto = _mapper.Map<PostDTO>(post);

            return _mapper.Map<PostDTO>(post);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(PostDTO postDto)
        {
            var updatePost = await _context.Posts.FirstAsync(post => post.Id == postDto.Id);

            _context.Entry(updatePost).State = EntityState.Detached;

            if (updatePost.PreviewId == null & postDto.PreviewImage != null)
            {
                _image = new Image
                {
                    URL = postDto.PreviewImage
                };
                _context.Images.Add(_image);
                await _context.SaveChangesAsync();
            }

            if (updatePost.PreviewId != null & postDto.PreviewImage != null)
            {
                _image = await _context.Images.FirstAsync(_image => _image.Id == updatePost.PreviewId);
                _image.URL = postDto.PreviewImage;
                _context.Entry(_image).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }

            var postEntity = _mapper.Map<Post>(postDto);

            if (_image != null)
            {
                postEntity.PreviewId = _image.Id;
            }

            _context.Entry(postEntity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            if (updatePost.PreviewId != null & postDto.PreviewImage == null)
            {
                _image = await _context.Images.FirstAsync(_image => _image.Id == updatePost.PreviewId);
                _context.Images.Remove(_image);
                await _context.SaveChangesAsync();
            }

            var post = await _context.Posts
                .Include(x => x.Preview)
                .Include(x => x.Comments)
                .Include(x => x.Reactions)
               .FirstAsync(post => post.Id == postDto.Id);

            var updatedPostDTO = _mapper.Map<PostDTO>(post);
            await _postHub.Clients.All.SendAsync("UpdatedPost", updatedPostDTO);

            return updatedPostDTO;
        }

        public async Task DeletePost(int postId)
        {
            var deletedPost = await _context.Posts
                .Include(x => x.Preview)
                .Include(x => x.Comments)
                .Include(x => x.Reactions)
               .FirstAsync(post => post.Id == postId);

            _context.Posts.Remove(deletedPost);
            await _context.SaveChangesAsync();

            if (deletedPost.PreviewId != null)
            {
                _image = await _context.Images.FirstAsync(x => x.Id == deletedPost.PreviewId);
                _context.Images.Remove(_image);
                await _context.SaveChangesAsync();
            }

            var post = _mapper.Map<PostDTO>(deletedPost);

            await _postHub.Clients.All.SendAsync("DeletedPost", post);
        }
    }
}
